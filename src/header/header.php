<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
  <link rel="stylesheet" href="../assets/style.css">
</head>

<body>
  <nav class="navbar navbar-expand-lg navbar-light bg-light">
    <div class="container-fluid">
      <button class="navbar-toggler" type="button" data-mdb-toggle="collapse" data-mdb-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <i class="fas fa-bars"></i>
      </button>
      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav me-auto mb-2 mb-lg-0">
          <li class="nav-item">
            <a type="summit" class="nav-link" href="/">หน้าเเรก</a>
          </li>
          <li class="nav-item">
            <a type="summit" class="nav-link" href="/all-user.php">สมาชิกทั้งหมด</a>
          </li>
          <li class="nav-item">
            <a type="submit" class="nav-link" href="/user-chat.php">หน้าเเชท</a>
          </li>
        </ul>
      </div>
      <?php
       if (isset($_SESSION['id'])) {    ?>
        <div class='dropdown'>
        <button class='dropbtn'> <i class='fas fa-user-circle'></i>  <?php
        echo $_SESSION['firstname'] ?> </button>
          <div class='dropdown-content'>
              <a  href='profile-setting.php' role='button'>Profile 
              <i class='fas fa-user-alt'></i></a>
              <a href='logout.php' role='button'>Logout 
              <i class='far fa-sign-out'></i></a>
          </div>
      </div> 
     <?php }  else {  ?>
        <div class='dropdown'>
                <button class='dropbtn'>เข้าสู่ระบบ!</button>
                  <div class='dropdown-content'>
                      <a  href='form-login.php' role='button'>Login<i class='fad fa-sign-in-alt'></i></a>
                      <a  href='form-register.php' role='button'>Register <i class='fas fa-registered'></i></a>
                  </div>
              </div> 
     <?php }
      ?>
  </nav>
</body>
<script src="../assets/js/popper.min.js"></script>
<script src="../assets/font/fontawesome/js/all.min.js"></script>

</html>
