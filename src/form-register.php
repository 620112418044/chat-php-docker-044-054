<?php
session_start();
ob_start();
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Register</title>
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Zenh87qX5JnK2Jl0vWa8Ck2rdkQ2Bzep5IDxbcnCeuOxjzrPF/et3URy9Bv1WTRi" crossorigin="anonymous">
  <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

</head>

<body>
  <div class="container mt-5">
    <div class="card" style="width: 100%; ">
      <div class="card-body">
        <form method="post" action="" enctype="multipart/form-data" class="mx-1 mx-md-4">
          <div class="d-flex flex-row align-items-center mb-4">
            <i class="fas fa-user fa-lg me-3 fa-fw"></i>
            <div class="form-outline flex-fill mb-0">
              <label class="form-label" for="form3Example1c">Username</label>
              <input type="text" name="username" id="form3Example1c" class="form-control" />
            </div>
          </div>
          <div class="d-flex flex-row align-items-center mb-4">
            <i class="fas fa-envelope fa-lg me-3 fa-fw"></i>
            <div class="form-outline flex-fill mb-0">
              <label class="form-label" for="form3Example3c">Password</label>
              <input type="password" name="password" id="form3Example3c" class="form-control" />
            </div>
          </div>
          <div class="d-flex flex-row align-items-center mb-4">
            <i class="fas fa-lock fa-lg me-3 fa-fw"></i>
            <div class="form-outline flex-fill mb-0">
              <label class="form-label" for="form3Example4c">Firstname</label>
              <input type="text" name="firstname" id="form3Example4c" class="form-control" />

            </div>
          </div>

          <div class="d-flex flex-row align-items-center mb-4">
            <i class="fas fa-key fa-lg me-3 fa-fw"></i>
            <div class="form-outline flex-fill mb-0">
              <label class="form-label" for="form3Example4cd">Lastname</label>
              <input type="text" name="lastname" id="form3Example4cd" class="form-control" />
            </div>
          </div>
          <div class="d-flex flex-row align-items-center mb-4">
            <i class="fas fa-key fa-lg me-3 fa-fw"></i>
            <div class="form-outline flex-fill mb-0">
              <label class="form-label" for="form3Example4cd">รูปโปรไฟล์</label>
              <input type="file" name="image" class="form-control" id="image" accept=".jpg, .jpeg, .png" value="">
            </div>
          </div>
          <div class="d-flex justify-content-center mx-4 mb-3 mb-lg-4">
            <p>หากมีบัญชีเเล้ว <a href="form-login.php">คลิ้กที่นี่</a>เพื่อเข้าสู่ระบบ</p>
          </div>
          <div class="d-flex justify-content-center mx-4 mb-3 mb-lg-4">
            <input type="submit" name="submit" class="btn btn-success">
          </div>
        </form>
      </div>
    </div>
  </div>
</body>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<?php

require "server.php";
if (isset($_POST['submit'])) {
  $username = $_POST['username'];
  $password = md5($_POST['password']);
  $firstname = $_POST['firstname'];
  $lastname = $_POST['lastname'];
  $active = 'Y';
  $create_at = date("Y-m-d H:i:s");
  if ($_FILES["image"]["error"] == 4) {
    echo
    "<script> alert('Image Does Not Exist'); </script>";
  } else {
    $fileName = $_FILES["image"]["name"];
    $fileSize = $_FILES["image"]["size"];
    $tmpName = $_FILES["image"]["tmp_name"];

    $validImageExtension = ['jpg', 'jpeg', 'png'];
    $imageExtension = explode('.', $fileName);
    $imageExtension = strtolower(end($imageExtension));
    if (!in_array($imageExtension, $validImageExtension)) {
      echo
      "
      <script>
        alert('Invalid Image Extension');
      </script>
      ";
    } else if ($fileSize > 1000000) {
      echo
      "
      <script>
        alert('ไฟล์รูปภาพใหญ่เกินไป');
      </script>
      ";
    } else {
      $newImageName = uniqid();
      $newImageName .= '.' . $imageExtension;

      move_uploaded_file($tmpName, 'assets/images/' . $newImageName);
      $sql = " INSERT INTO `tb_user`(`username`, `password`, `firstname`, `lastname`,`active`,`img`,`create_at`)";
      $sql .= " VALUES ('$username','$password','$firstname','$lastname','$active','$newImageName','$create_at')";
      mysqli_query($conn, $sql);
      echo
      "
      <script>
        alert('ลงทะเบียนสำเร็จ');
        document.location.href = 'form-login.php';
      </script>
      ";
    }
  }



















  // $img_profile = $_POST['image'];
  // if (empty($username) || empty($password) || empty($firstname) || empty($lastname)) {
  //   echo "<script>
  //           Swal.fire(
  //               'The Internet?',
  //               'That thing is still around?',
  //               'question'
  //             )
  //       </script>";
  //   #header('location: form-register.php');
  // } else {
  //   $sql = " INSERT INTO `tb_user`(`username`, `password`, `firstname`, `lastname`,`active`)";
  //   $sql .= " VALUES ('$username','$password','$firstname','$lastname','$active')";
  //   $result = mysqli_query($conn, $sql);
  //   // echo "
  //   //         <script>
  //   //         Swal.fire({
  //   //             position: 'top-end',
  //   //             icon: 'success',
  //   //             title: 'Your work has been saved',
  //   //             showConfirmButton: false,
  //   //             timer: 1500
  //   //           })</script>";
  //   header('location: form-login.php');
  // }

}

?>

</html>