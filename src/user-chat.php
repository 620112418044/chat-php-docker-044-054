<?php
session_start();

require "server.php";
require "get-user.php";
if (!isset($_SESSION['id'])) {
  echo
    "<script> alert('ยังไม่ได้เข้าสู่ระบบ'); 
    window.location.href = 'form-login.php';
    </script>";
    //header("location: form-login.php");
  
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>หน้าเเชท</title>
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Zenh87qX5JnK2Jl0vWa8Ck2rdkQ2Bzep5IDxbcnCeuOxjzrPF/et3URy9Bv1WTRi" crossorigin="anonymous">

  <?php
  require "./header/header.php"
  ?>
</head>

<body>
  <section style="background-color: #eee;">
    <div class="container py-5">

      <div class="row">

        <div class="col-md-6 col-lg-5 col-xl-4 mb-4 mb-md-0">

          <h5 class="font-weight-bold mb-3 text-center text-lg-start">Member</h5>
          <?php
          $all_user = new User();
          $getvalue = $all_user->chatMember($_SESSION['id']);
          foreach ($getvalue as $row) { ?>
            <div class="card">
              <div class="card-body">
                <ul class="list-unstyled mb-0">
                  <li class="p-2 border-bottom" style="background-color: #eee;">
                    <a href="#!" class="d-flex justify-content-between">
                      <div class="d-flex flex-row">
                        <div class="pt-1">
                          <p class="fw-bold mb-0">
                            <?php
                            echo $row['firstname'] . ' ' . $row['lastname']
                            ?>
                          </p>
                          <p class="small text-muted">ข้อความล่าสุด</p>
                        </div>
                      </div>
                      <div class="pt-1">
                        <p class="small text-muted mb-1">active = Y</p>
                        <span class="badge bg-danger float-end">1</span>
                      </div>
                    </a>
                  </li>
                </ul>

              </div>
            </div>
          <?php   } ?>
        </div>
        <div class="col-md-6 col-lg-7 col-xl-8">
          <?php
          $all_user = new User();
          $getvalue = $all_user->chatMember($_SESSION['id']);
          foreach ($getvalue as $row) { ?>

          ?>
          <ul class="list-unstyled">
            <li class="d-flex justify-content-between mb-4">
              <img src="https://mdbcdn.b-cdn.net/img/Photos/Avatars/avatar-6.webp" alt="avatar" class="rounded-circle d-flex align-self-start me-3 shadow-1-strong" width="60">
              <div class="card">
                <div class="card-header d-flex justify-content-between p-3">
                  <p class="fw-bold mb-0">Brad Pitt</p>
                  <p class="text-muted small mb-0"><i class="far fa-clock"></i> 12 mins ago</p>
                </div>
                <div class="card-body">
                  <p class="mb-0">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut
                    labore et dolore magna aliqua.
                  </p>
                </div>
              </div>
            </li>
            <li class="d-flex justify-content-between mb-4">
              <div class="card w-100">
                <div class="card-header d-flex justify-content-between p-3">
                  <p class="fw-bold mb-0">Lara Croft</p>
                  <p class="text-muted small mb-0"><i class="far fa-clock"></i> 13 mins ago</p>
                </div>
                <div class="card-body">
                  <p class="mb-0">
                    Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque
                    laudantium.
                  </p>
                </div>
              </div>
              <img src="https://mdbcdn.b-cdn.net/img/Photos/Avatars/avatar-5.webp" alt="avatar" class="rounded-circle d-flex align-self-start ms-3 shadow-1-strong" width="60">
            </li>
            <li class="d-flex justify-content-between mb-4">
              <img src="https://mdbcdn.b-cdn.net/img/Photos/Avatars/avatar-6.webp" alt="avatar" class="rounded-circle d-flex align-self-start me-3 shadow-1-strong" width="60">
              <div class="card">
                <div class="card-header d-flex justify-content-between p-3">
                  <p class="fw-bold mb-0">Brad Pitt</p>
                  <p class="text-muted small mb-0"><i class="far fa-clock"></i> 10 mins ago</p>
                </div>
                <div class="card-body">
                  <p class="mb-0">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut
                    labore et dolore magna aliqua.
                  </p>
                </div>
              </div>
            </li>
            <?php
          } 
            ?>
            <li class="bg-white mb-3">
              <div class="form-outline">
                <form action="">
                  <textarea class="form-control" id="textAreaExample2" rows="4"></textarea>
                </form>
                <label class="form-label" for="textAreaExample2">Message</label>

              </div>
              <button type="submit" class="btn btn-info btn-rounded float-end">Send</button>
            </li>

          </ul>

        </div>

      </div>

    </div>
  </section>
</body>

</html>