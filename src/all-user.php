<?php
require "server.php";
session_start();
require "get-user.php";
#เช็คLogin
if (!isset($_SESSION['id'])) {
  echo
  "<script> alert('ยังไม่ได้เข้าสู่ระบบ'); 
    window.location.href = 'form-login.php';
    </script>";
  // header("location: form-login.php");
}




?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Zenh87qX5JnK2Jl0vWa8Ck2rdkQ2Bzep5IDxbcnCeuOxjzrPF/et3URy9Bv1WTRi" crossorigin="anonymous">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.1/css/all.min.css" integrity="sha256-2XFplPlrFClt0bIdPgpz8H7ojnk10H69xRqd9+uTShA=" crossorigin="anonymous" />
  <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
  <title>All-Members</title>

</head>

<?php
require "./header/header.php"
?>
</head>


<body>

  <div class="container">
    <div class="col-12 ">
      <div class="row">
        <div class="col-md-12">
          <div class="user-dashboard-info-box table-responsive mb-0 bg-white p-4 shadow-sm">
            <table class="table manage-candidates-top mb-0">
              <thead>
                <tr>
                  <th>ชื่อ</th>
                  <th class="text-center">นามสกุล</th>
                  <th class="text-center">เข้าร่วมเมื่อ</th>
                  <th class="text-center">Action</th>
                  <th class="text-center">รูปโปรไฟล์</th>
                </tr>
              </thead>
              <tbody>
                <?php
                $all_user = new User();
                $getvalue = $all_user->find_all();
                while ($row = mysqli_fetch_array($getvalue)) { ?>
                  <tr class="candidates-list">
                    <td class="title">
                      <div class="candidate-list-details">
                        <div class="candidate-list-info">
                          <div class="candidate-list-title">
                            <p>
                              <?php
                              echo  $row['firstname']
                              ?>
                            </p>
                          </div>
                        </div>
                      </div>
                    </td>
                    <td class="candidate-list-favourite-time text-center">
                      <div class="candidate-list-details">
                        <div class="candidate-list-info">
                          <div class="candidate-list-title">
                            <p>
                              <?php
                              echo  $row['lastname']
                              ?>
                            </p>
                          </div>
                        </div>
                      </div>

                    </td>
                    <td class="candidate-list-favourite-time text-center">
                      <div class="candidate-list-details">
                        <div class="candidate-list-info">
                          <div class="candidate-list-title">
                            <p>
                              <?php
                              echo  $row['create_at']
                              ?>
                            </p>
                          </div>
                        </div>
                      </div>
                    </td>
                    <td class="candidate-list-favourite-time text-center">
                      <div class="candidate-list-details">
                        <div class="candidate-list-info">
                          <div class="candidate-list-title">
                            <a href=""><i class="far fa-user-md-chat"></i></a>
                            <a href=""><i class="fas fa-window-minimize"></i></i></a>
                          </div>
                        </div>
                      </div>
                    </td>
                    <td class="candidate-list-favourite-time text-center">
                      <div class="candidate-list-details">
                        <div class="candidate-list-info">
                          <div class="candidate-list-title">
                  <?php
                        if(empty($row['img'])){?>

                       <p>ผู้ใช้ไม่ได้อัพโหลดรุปภาพ</p>
                         
                       <?php } else{ ?>
                        <img src="./assets/images/<?php echo $row['img'] ?>" alt="" width="100px" height="100px">
                      <?php }
                  ?>
                          </div>
                        </div>
                      </div>
                    </td>
                  </tr>
                <?php   } ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
  </div>
</body>
<script src="../assets/js/popper.min.js"></script>
<script src="../assets/font/fontawesome/js/all.min.js"></script>

</html>