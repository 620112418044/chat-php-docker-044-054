-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: mysql_db
-- Generation Time: Nov 29, 2022 at 10:57 AM
-- Server version: 8.0.31
-- PHP Version: 8.0.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `Chat_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_chat`
--

CREATE TABLE `tb_chat` (
  `id` int NOT NULL,
  `msg` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `from_id` int NOT NULL,
  `to_id` int NOT NULL,
  `create_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `read_status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `tb_chat`
--

INSERT INTO `tb_chat` (`id`, `msg`, `from_id`, `to_id`, `create_at`, `read_status`) VALUES
(1, 'hello', 2, 3, '2022-11-23 16:50:58', 'N'),
(2, 'TEST TEST', 3, 2, '2022-11-23 16:51:58', 'N');

-- --------------------------------------------------------

--
-- Table structure for table `tb_user`
--

CREATE TABLE `tb_user` (
  `id` int NOT NULL,
  `username` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `firstname` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `lastname` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `active` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `img` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `create_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `tb_user`
--

INSERT INTO `tb_user` (`id`, `username`, `password`, `firstname`, `lastname`, `active`, `img`, `create_at`) VALUES
(1, 'admin', 'b6952dad2a06458b545f07866a27b069', 'korn', 'naja', 'Y', '', '2022-11-23 09:19:15'),
(2, 'korn', '827ccb0eea8a706c4c34a16891f84e7b', 'korn', 'jamikorn', 'Y', '637de8a1c587e.jpg', '2022-11-23 09:32:17'),
(3, 'kan11', '827ccb0eea8a706c4c34a16891f84e7b', 'kan11', 'korn11', 'Y', '637deaf757640.png', '2022-11-23 09:42:15'),
(4, 'opop', '827ccb0eea8a706c4c34a16891f84e7b', 'จามิกร', 'ชนะกิจเจริญ', 'Y', '6385d51d33075.jpg', '2022-11-29 09:47:09'),
(5, 'korn', '827ccb0eea8a706c4c34a16891f84e7b', 'จามิกร', 'ชนะกิจเจริญ', 'Y', '6385d5439dbe5.jpg', '2022-11-29 09:47:47'),
(6, 'kornddd', '827ccb0eea8a706c4c34a16891f84e7b', 'จามิกร', 'ชนะกิจเจริญ', 'Y', '6385d5c793e6c.png', '2022-11-29 09:49:59'),
(7, 'kornddd', '827ccb0eea8a706c4c34a16891f84e7b', 'จามิกร', 'ชนะกิจเจริญ', 'Y', '6385d612ae67a.png', '2022-11-29 09:51:14'),
(8, 'korn123', '827ccb0eea8a706c4c34a16891f84e7b', 'จามิกร', 'ชนะกิจเจริญ', 'Y', '6385d61ed2fbf.jpg', '2022-11-29 09:51:26'),
(9, 'korn123', '827ccb0eea8a706c4c34a16891f84e7b', 'จามิกร', 'ชนะกิจเจริญ', 'Y', '6385d6606516d.jpg', '2022-11-29 09:52:32'),
(10, 'kornkk', '827ccb0eea8a706c4c34a16891f84e7b', 'จามิกร', 'ชนะกิจเจริญ', 'Y', '6385d66be3251.jpg', '2022-11-29 09:52:43'),
(11, 'kornkk', '827ccb0eea8a706c4c34a16891f84e7b', 'จามิกร', 'ชนะกิจเจริญ', 'Y', '6385d69a753c6.jpg', '2022-11-29 09:53:30'),
(12, 'korn44', '827ccb0eea8a706c4c34a16891f84e7b', 'จามิกร', 'ชนะกิจเจริญ', 'Y', '6385d6a7a523a.jpg', '2022-11-29 09:53:43'),
(13, 'korn44', '827ccb0eea8a706c4c34a16891f84e7b', 'จามิกร', 'ชนะกิจเจริญ', 'Y', '6385d6c6804f1.jpg', '2022-11-29 09:54:14'),
(14, 'korn66', '827ccb0eea8a706c4c34a16891f84e7b', 'จามิกร', 'korn', 'Y', '6385d6e5152cb.jpg', '2022-11-29 09:54:45'),
(15, 'kannahe', '827ccb0eea8a706c4c34a16891f84e7b', 'kan', 'kankuy', 'Y', '6385daec44b28.jpg', '2022-11-29 10:11:56'),
(16, 'kannahe', '827ccb0eea8a706c4c34a16891f84e7b', 'kan', 'kankuy', 'Y', '6385db2c10896.jpg', '2022-11-29 10:13:00'),
(17, 'kannahe42', '827ccb0eea8a706c4c34a16891f84e7b', 'kan', 'kankuy555', 'Y', '6385db3ca6c9b.jpg', '2022-11-29 10:13:16');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_chat`
--
ALTER TABLE `tb_chat`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_user`
--
ALTER TABLE `tb_user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_chat`
--
ALTER TABLE `tb_chat`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tb_user`
--
ALTER TABLE `tb_user`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
